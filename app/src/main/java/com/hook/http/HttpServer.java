package com.hook.http;

import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.hook.project.MeiRiYouXian;
import com.hook.project.MeiTuanServer;
import com.hook.project.TaoBaoHttpServer;
import com.zhihu.android.cloudid.CloudIDHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

public class HttpServer extends NanoHTTPD {

    public HttpServer(int port){
        super(port);
    }

    public HttpServer(String hostname, int port) {
        super(hostname, port);
    }

    //开放http服务请求到这里

    @Override
    public Response serve(IHTTPSession session){
        Method method = session.getMethod();
        Map<String, String> header = session.getHeaders();
        Map<String, String> parameters = session.getParms();
        String uri = session.getUri().substring(1);
        Map<String,Object> responseMap = new HashMap<>();
        if(NanoHTTPD.Method.GET.equals(method)){
            //get方式
            String queryParams = session.getQueryParameterString();
            Log.e("weifang","params:"+queryParams);
            if("meituan".equals(parameters.get("type"))){
                try{
                    return MeiTuanServer.getSecret(parameters);
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e("EDXposed","MeiTuan:"+e.getMessage());
                }
            } else if ("zhihu".equals(parameters.get("type"))) {
                String res = CloudIDHelper.getInstance().encrypt("2", null, null,"app_build=677&app_version=5.17.1&bt_ck=1&bundle_id=com.zhihu.android&cp_ct=8&cp_fq=1800000&cp_tp=0&cp_us=100.0&d_n=Redmi%2010X%204G&fr_mem=0&fr_st=95568&latitude=0.0&longitude=0.0&mc_ad=98%3AF6%3A21%3A58%3AF6%3A4D&mcc=cn&nt_st=1&ph_br=Redmi&ph_md=M2003J15SC&ph_os=Android%2010&ph_sn=unknown&pvd_nm=%E4%B8%AD%E5%9B%BD%E8%81%94%E9%80%9A&tt_mem=17&tt_st=107513&tz_of=28800","1355","1610027819","dd49a835-56e7-4a0f-95b5-efd51ea5397f");
                responseMap.put("code", 200);
                responseMap.put("msg", res);
            }else{
                responseMap.put("code",500);
                responseMap.put("msg","未知方法名称,请传入method的值.");
            }
            return newFixedLengthResponse(Response.Status.OK,"Application/json", JSONObject.toJSONString(responseMap));
        }else if(NanoHTTPD.Method.POST.equals(method)){
            Map<String, String> files = new HashMap<>();
            try {
                session.parseBody(files);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ResponseException e) {
                e.printStackTrace();
            }
            if("taobao".equals(parameters.get("method"))){
                String postData = files.get("postData");
                return TaoBaoHttpServer.enter(parameters);
            }else if("meiriyouxian".equals(parameters.get("method"))){
                String postData = files.get("postData");
                Log.e("EDXposed","post数据为: " + postData);
                return MeiRiYouXian.getSign(postData);
            }
            //post方式
            responseMap.put("code",500);
            responseMap.put("msg","未知方法名称,请传入method的值.");
            return newFixedLengthResponse(Response.Status.OK,"Application/json", JSONObject.toJSONString(responseMap));
        }
        responseMap.put("code",500);
        responseMap.put("msg","不支持GET OR POST 外的其他方法.");
        return newFixedLengthResponse(Response.Status.OK,"Application/json", JSONObject.toJSONString(responseMap));
    }

}
