package com.hook.tools;


public class MyThreadLocal extends ThreadLocal<String> {
    private static volatile com.hook.tools.MyThreadLocal instance = null;
    public static com.hook.tools.MyThreadLocal getInstance() {
        if (instance == null) {
            synchronized (com.hook.tools.MyThreadLocal.class) {
                if (instance == null) {
                    instance = new com.hook.tools.MyThreadLocal();
                }
            }
        }
        return instance;
    }
}