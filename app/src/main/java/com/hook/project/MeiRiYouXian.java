package com.hook.project;

import android.content.Context;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.hook.tools.initXposedHook;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import de.robv.android.xposed.XposedHelpers;
import fi.iki.elonen.NanoHTTPD;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class MeiRiYouXian {

    /**
     * 加密签名参数
     * @return
     */
    public static NanoHTTPD.Response getSign(String postData){
        String str = "{msg:\"未知方法名称\"}";
        //获取每日优鲜加密对象类
        //获取每日优鲜加密函数 Context参数
        Object meiriyouxianContext = initXposedHook.meiriyouxianContext;
        String data = JSONObject.parseObject(postData).getString("data");
        long time = JSONObject.parseObject(postData).getLong("time");
        Class<?> clazz = initXposedHook.meiriyouxinSecurityClass;
        try {
            Map<String,String> map = new HashMap<>();
            Method aMethod = clazz.getMethod("a", Context.class,byte[].class);
            String res = aMethod.invoke(null,meiriyouxianContext,data.getBytes("UTF-8")).toString();
            Log.e("EDXposed", "加密结果为   :" + res);
            map.put("mfSign", res);
            str = JSONObject.toJSONString(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFixedLengthResponse(NanoHTTPD.Response.Status.OK, "application/json", str);
    }


}
