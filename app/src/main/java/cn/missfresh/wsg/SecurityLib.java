package cn.missfresh.wsg;

import android.content.Context;


public class SecurityLib {
    private static boolean a = true;
    private static boolean b;
    private static final Object c = new Object();

    private static native int nativeInit(Context context, String str);

    public static native String nativeSign(Context context, long j, byte[] bArr);

    static {
        b = false;
        try {
            System.loadLibrary("sign");
            b = true;
        } catch (Exception e) {
            b = false;
            e.printStackTrace();
        }
    }

    public static String a(Context context, byte[] bArr) {
        if (!a) {
            String valueOf = String.valueOf(103);
            return valueOf;
        }
        String nativeSign = nativeSign(context, System.currentTimeMillis(), bArr);
        return nativeSign;
    }

    public static int a(Context context, String str) {
        int i = 0;
        if (!b || context == null) {
            int i2 = 103;
            return i2;
        }
        synchronized (c) {
            try {
                if (!a) {
                    i = nativeInit(context, str);
                    if (i == 0) {
                        a = true;
                    }
                    c.notifyAll();
                }
            } finally {
            }
        }
        return i;
    }
}